package psk1.domain.entities;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

/**
 * Created by Mantas on 3/6/2016.
 */
@Entity
@Table(name = "TEAM", schema = "APP", catalog = "")
public class TeamEntity {
    private int id;
    private String name;

    @Id
    @Column(name = "ID")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ElementCollection
    @CollectionTable(name = "PLAYER", joinColumns = @JoinColumn(name = "TEAM_ID"))
    public Collection<PlayerEntity> Players;

    @ManyToMany
    @JoinTable(name = "TEAM_SPONSORS",
            joinColumns = @JoinColumn(name = "TEAM_ID", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "SPONSOR_ID", referencedColumnName = "ID"))
    public Collection<SponsorEntity> Sponsors;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TeamEntity that = (TeamEntity) o;

        if (id != that.id) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
