package psk1.domain.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Mantas on 3/6/2016.
 */
@Embeddable
@Table(name = "TEAM_SPONSORS", schema = "APP", catalog = "")
public class TeamSponsorsEntity implements Serializable{
    private int teamId;
    private int sponsorId;

    @Id
    @Column(name = "TEAM_ID")
    public int getTeamId() {
        return teamId;
    }

    public void setTeamId(int teamId) {
        this.teamId = teamId;
    }

    @Id
    @Column(name = "SPONSOR_ID")
    public int getSponsorId() {
        return sponsorId;
    }

    public void setSponsorId(int sponsorId) {
        this.sponsorId = sponsorId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TeamSponsorsEntity that = (TeamSponsorsEntity) o;

        if (teamId != that.teamId) return false;
        if (sponsorId != that.sponsorId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = teamId;
        result = 31 * result + sponsorId;
        return result;
    }
}
