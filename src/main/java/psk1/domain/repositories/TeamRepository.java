package psk1.domain.repositories;

import psk1.domain.entities.TeamEntity;

import java.util.Collection;

/**
 * Created by Mantas on 3/6/2016.
 */
public interface TeamRepository {
    void create(TeamEntity team);
    Collection<TeamEntity> get();
    TeamEntity get(int id);
    void update(TeamEntity team);
    void delete(int id);
}
