package psk1.domain.repositories;

import psk1.domain.entities.PlayerEntity;

import java.util.Collection;

/**
 * Created by Mantas on 2/21/2016.
 */
public interface PlayerRepository {

    void create(PlayerEntity player);
    Collection<PlayerEntity> get();
    PlayerEntity get(int id);
    void update(PlayerEntity player);
    void delete(int id);
}
