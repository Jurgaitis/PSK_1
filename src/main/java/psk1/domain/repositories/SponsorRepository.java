package psk1.domain.repositories;

import psk1.domain.entities.SponsorEntity;

import java.util.Collection;

/**
 * Created by Mantas on 2/21/2016.
 */
public interface SponsorRepository {
    void create(SponsorEntity sponsor);
    Collection<SponsorEntity> get();
    SponsorEntity get(int id);
    void update(SponsorEntity sponsor);
    void delete(int id);
}
