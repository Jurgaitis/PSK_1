package psk1.webapi.controllers;

import psk1.domain.entities.TeamEntity;
import psk1.domain.repositories.TeamRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by Mantas on 2/16/2016.
 */

@Stateless
@Path("team")
public class TeamController {

    @Inject
    TeamRepository repository;


    @GET
    @Path("/getAll")
    @Produces(MediaType.APPLICATION_JSON)
    public List<TeamEntity> GetAll() {
        return (List) repository.get();
    }

    @GET
    @Path("/get/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public TeamEntity Get(@PathParam("id")int id){
        return repository.get(id);
    }

    @POST
    @Path("/save")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public TeamEntity Save(TeamEntity team){
        repository.create(team);

        return team;
    }

    @POST
    @Path("update")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void Update(TeamEntity team){
        repository.update(team);
    }

    @POST
    @Path("/delete/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public TeamEntity Delete(@PathParam("id")int id){
        TeamEntity team = repository.get(id);

        repository.delete(id);

        return team;
    }

}
