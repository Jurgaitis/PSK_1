package psk1.enums;

/**
 * Created by Mantas on 3/6/2016.
 */
public enum Position {
        Defender(0, "Defender"), Midfielder(1, "Midfielder"), Forward(2, "Forward"), Goalkeeper(3, "Goalkeeper");

    private final String name;
    private int value;

        Position(int value, String name) {
            this.value = value;
            this.name = name;
        }

        public int getValue() { return value; }
        public String getName() { return name; }

        public static Position parse(int id) {
            Position position = null; // Default
            for (Position item : Position.values()) {
                if (item.getValue()==id) {
                    position = item;
                    break;
                }
            }
            return position;
        }
}
