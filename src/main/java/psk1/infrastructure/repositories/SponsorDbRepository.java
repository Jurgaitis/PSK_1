package psk1.infrastructure.repositories;

import psk1.domain.entities.PlayerEntity;
import psk1.domain.entities.SponsorEntity;
import psk1.domain.repositories.SponsorRepository;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.Collection;

/**
 * Created by Mantas on 2/28/2016.
 */

@Stateless
public class SponsorDbRepository implements SponsorRepository {

    @PersistenceContext(unitName = "PSK_1_Persistence")
    private EntityManager entityManager;

    public void create(SponsorEntity sponsor) {
        entityManager.persist(sponsor);
    }

    public Collection<SponsorEntity> get() {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<SponsorEntity> criteriaQuery = builder.createQuery(SponsorEntity.class);
        criteriaQuery.from(SponsorEntity.class);

        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    public SponsorEntity get(int id) {
        return entityManager.find(SponsorEntity.class, id);
    }

    public void update(SponsorEntity sponsor) {
        entityManager.merge(sponsor);
    }

    public void delete(int id) {
        SponsorEntity user = get(id);
        if(user != null){
            entityManager.remove(user);
        }
    }
}
