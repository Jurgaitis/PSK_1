package psk1.infrastructure.repositories;

import psk1.domain.entities.SponsorEntity;
import psk1.domain.entities.TeamEntity;
import psk1.domain.repositories.TeamRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.Collection;

/**
 * Created by Mantas on 3/6/2016.
 */
public class TeamDbRepository implements TeamRepository {

    @PersistenceContext(unitName = "PSK_1_Persistence")
    private EntityManager entityManager;

    public void create(TeamEntity team) {
        entityManager.persist(team);
    }

    public Collection<TeamEntity> get() {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<TeamEntity> criteriaQuery = builder.createQuery(TeamEntity.class);
        criteriaQuery.from(TeamEntity.class);

        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    public TeamEntity get(int id) {
        return entityManager.find(TeamEntity.class, id);
    }

    public void update(TeamEntity team) {
        entityManager.merge(team);
    }

    public void delete(int id) {
        TeamEntity team = get(id);
        if(team != null){
            entityManager.remove(team);
        }
    }
}
