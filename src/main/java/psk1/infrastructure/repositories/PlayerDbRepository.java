package psk1.infrastructure.repositories;

import psk1.domain.entities.PlayerEntity;
import psk1.domain.repositories.PlayerRepository;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

/**
 * Created by Mantas on 2/21/2016.
 */

@Stateless
public class PlayerDbRepository implements PlayerRepository {

    @PersistenceContext(unitName = "PSK_1_Persistence")
    private EntityManager entityManager;

    public void create(PlayerEntity player) {
        entityManager.persist(player);
    }

    public List<PlayerEntity> get() {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<PlayerEntity> criteriaQuery = builder.createQuery(PlayerEntity.class);
        criteriaQuery.from(PlayerEntity.class);

        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    public PlayerEntity get(int id) {
        return entityManager.find(PlayerEntity.class, id);
}

    public void update(PlayerEntity player) {
        entityManager.merge(player);
    }

    public void delete(int id) {
        PlayerEntity player = get(id);
        if(player != null){
            entityManager.remove(player);
        }
    }
}
