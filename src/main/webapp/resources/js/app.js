var app = angular.module('demoApp', ['ngRoute', 'ui.bootstrap']);

app.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    $routeProvider.when('/',
        {
            controller: 'threadController',
            templateUrl: 'html/threadListView.html'
        }).when('/thread/:id',
        {
            controller: 'threadDetailsController',
            templateUrl: 'html/threadDetailView.html'
        }).otherwise({redirectTo: '/'});

    $locationProvider.html5Mode({
        enabled: true
    });
}]);
