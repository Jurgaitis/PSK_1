angular.module('demoApp').service('threadService', ['$http', function ($http) {
    var baseUrl = 'rest/threads/';

    this.getAll = function() {
        return $http.get(baseUrl + 'getAll');
    };

    this.get = function(id) {
        return $http.get(baseUrl + 'get/' + id);
    };

    this.save = function (thread) {
        return $http.post(baseUrl + 'saveThread', thread);
    };

    this.saveComment = function (threadId, comment) {
        return $http.post(baseUrl + 'saveComment/' + threadId, comment);
    }
}]);