angular.module('demoApp').controller('threadController', ['$scope', 'threadService', '$uibModal', function ($scope, threadService, $uibModal) {
    threadService.getAll()
        .success(function (threads) {
            $scope.threads = threads;
        });

    $scope.animationsEnabled = true;

    $scope.open = function (size) {
        var modal = $uibModal.open({
            templateUrl: 'newThreadModal.html',
            controller: 'newThreadModalController',
            size: size
        });

        modal.result.then(function (result) {
            $scope.threads.push(result);
        });
    };
}]);

angular.module('demoApp').controller('newThreadModalController', ['$scope', '$uibModalInstance', 'threadService', function ($scope, $uibModalInstance, threadService) {
    $scope.ok = function () {
        $scope.thread.id = null;
        $scope.thread.timestamp = new Date();
        $scope.thread.comments = [];
        threadService.save($scope.thread).success(function (result) {
            $scope.thread = result;
        });

        $uibModalInstance.close($scope.thread);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

angular.module('demoApp').controller('threadDetailsController', ['$scope', '$routeParams', 'threadService', function ($scope, $routeParams, threadService) {
    threadService.get($routeParams.id).success(function (result) {
        $scope.thread = result;
    });

    $scope.saveComment = function () {
        $scope.newComment.timestamp = new Date();
        $scope.newComment.thread = $scope.thread;
        threadService.saveComment($scope.thread.id, $scope.newComment).success(function(result) {
            $scope.thread.comments.push(result);
            $scope.newComment = {};
        })
    }
}]);